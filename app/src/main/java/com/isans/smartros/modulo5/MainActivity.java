package com.isans.smartros.modulo5;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.isans.smartros.modulo5.R;

import org.ros.android.smartRosActivity;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// ref: https://code.google.com/p/usb-serial-for-android/source/browse/UsbSerialExamples/src
// /com/hoho/android/usbserial/examples/SerialConsoleActivity.java
public class MainActivity extends smartRosActivity {

    private static UsbSerialDriver sDriver = null;
    private UsbManager mUsbManager;
    private byte[] rx = new byte[128];
    private int rxIndex = 0;
    private int fps = 1;
    private Handler handler;
    private Handler delayHandler;
    private Protocol m5 = null;
    private static final String TAG = "MainActivity";
    public MainActivity() {
        super("Modulo5 Node");
    }
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private SerialInputOutputManager mSerialIoManager;

    private final SerialInputOutputManager.Listener mListener =
        new SerialInputOutputManager.Listener() {
            @Override
            public void onRunError(Exception e) {
                Log.d(TAG, "Runner stopped.");
                sDriver = null;
            }
            @Override
            public void onNewData(final byte[] data) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // parsing code
                        m5.USART1_RX_vect(data);
                    }
                });
            }
        };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        sDriver = UsbSerialProber.acquire(mUsbManager);
        m5 = new Protocol(sDriver);
        this.handler = new Handler();
        this.delayHandler = new Handler();
        Thread thread = new Thread(new Runnable() {
            public void run() {

                while (sDriver != null) {
                    try {
                        Thread.sleep(1000 / fps);
                        handler.post(new Runnable() {
                            public void run() {
                                // periodic writing code
                                StringBuffer recordValue = new StringBuffer();
                                if(m5.recvRespMessage(recordValue)){
                                    sendRespMessage(recordValue.toString());
                                }
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resumed, sDriver=" + sDriver);
        if (sDriver == null) {
            Log.d(TAG, "No serial device.");
        } else {
            try {
                sDriver.open();
                sDriver.setBaudRate(115200);// , 8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                try {
                    sDriver.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                sDriver = null;
                return ;
            } catch (Exception e){
                e.printStackTrace();
                Log.d(TAG, "Serial Open error");
                sDriver = null;
                return ;
            }
            Log.d(TAG, "Serial device: " + sDriver.getClass().getSimpleName());
        }
        onDeviceStateChange();
    }
    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }
    private void startIoManager() {
        if (sDriver != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(sDriver, mListener);
            mExecutor.submit(mSerialIoManager);
            final String title = String.format("Vendor %s Product %s",
                    HexDump.toHexString((short) sDriver.getDevice().getVendorId()),
                    HexDump.toHexString((short) sDriver.getDevice().getProductId()));
            Toast.makeText(this, "Found USB device: \n" + title, Toast.LENGTH_SHORT).show();
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        stopIoManager();
    }

    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        String[] parts = s.split("/");
        if(parts.length == 0) return ;
        switch(parts[1]){
            case "start_all":
                break;
            case "stop_all":
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "m5_velocity":
                if(boundary(parts, 3)){
                    m5.setWHEEL_TARGET_V(
                            (short) Integer.parseInt(parts[2]), // left
                            (short) Integer.parseInt(parts[3])  // right
                    );
                }
                break;
            case "m5_stop":
                if(boundary(parts, 1)){
                    m5.setWHEEL_TARGET_V(
                            (short) 0, // left
                            (short) 0  // right
                    );
                }
                break;
            case "m5_tilt":
                if(boundary(parts, 2)){
                    m5.setTILT_TARGET_A(
                            (short) Integer.parseInt(parts[2])
                    );
                }
                break;
            case "m5_servo":
                if(boundary(parts, 5)){
                    m5.setSERVO_TARGET_S_A(
                            (short) Integer.parseInt(parts[2]), // SPEED
                            (short) Integer.parseInt(parts[3]), // ID1's degree
                            (short) Integer.parseInt(parts[4]), // ID2's degree
                            (short) Integer.parseInt(parts[5])  // ID3's degree
                    );
                }
                break;
            case "m5_led":
                if(boundary(parts, 5)){
                    m5.setLED_M5(
                            (byte) Integer.parseInt(parts[2]),  // R
                            (byte) Integer.parseInt(parts[3]),  // G
                            (byte) Integer.parseInt(parts[4]),  // B
                            (byte) Integer.parseInt(parts[5])   // ON/OFF
                    );
                }
                break;
            case "m5_melody":
                if(boundary(parts, 2)){
                    byte param = 0;
                    if(parts[2].equals("play")) param = 1;
                    else if(parts[2].equals("previous")) param = 2;
                    else if(parts[2].equals("stop")) param = 3;
                    m5.setMELODY(param);
                }
                break;
            case "m5_buzzer":
                if(boundary(parts, 2)){
                    m5.setBUZZER(
                            (byte) Integer.parseInt(parts[2])
                    );
                }
                break;
            case "m5_bt_remocon":
                if(boundary(parts, 3)){
                    m5.setBT_REMOCON(
                            (short) Integer.parseInt(parts[2]), // x
                            (short) Integer.parseInt(parts[3]), // y
                            (short) 0,
                            (short) 0
                    );
                }
                break;
            default :
                break;
        }
    }
    private boolean boundary(String[] range, int idx){
        return (range.length > idx );
    }
}
