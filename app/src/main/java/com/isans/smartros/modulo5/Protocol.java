package com.isans.smartros.modulo5;

import com.hoho.android.usbserial.driver.UsbSerialDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bcc on 2015-06-05.
 */
public class Protocol {

    ///////////CMD Define/////////////

    public static final byte CMD_M_NOW = 0;	//	R/W
    public static final byte CMD_M_1Hz = 1;	//	R/W
    public static final byte CMD_M_5Hz = 2;	//	R/W
    public static final byte CMD_M_10Hz	= 3;	//	R/W

    public static final byte CMD_PLATFORM_SET = 11;	//	R/W
    public static final byte CMD_PLATFORM_STATUS	= 12;	//	R
    public static final byte CMD_ADMINISTOR_PACKET = 13;	//	admin
    public static final byte CMD_FLASH_D1 = 14;	//	R
    public static final byte CMD_FLASH_D2 = 15;	//	R

    public static final byte CMD_BT_REMOCON = 21;	//	R/W
    public static final byte CMD_WHEEL_TARGET_V = 22;	//	R/W
    public static final byte CMD_WHEEL_ACC_DEACC = 23;	//	R/W
    public static final byte CMD_WHEEL_CURRENT_V = 24;	//	R
    public static final byte CMD_TILT_TARGET_A = 25;	//	R/W
    public static final byte CMD_TILT_CURRENT_A	= 26;	//	R
    public static final byte CMD_SERVO_TARGET_S_A = 27;	//	R/W
    public static final byte CMD_SERVO_CURRENT_A = 28;	//	R
    public static final byte CMD_LED_M5 = 29;	//	R/W
    public static final byte CMD_DISTANCE_PSD = 30;	//	R
    public static final byte CMD_ODOMETRY = 31;	//	R
    public static final byte CMD_CLCD = 32;	//	R/W
    public static final byte CMD_BUTTON	= 33;	//	R
    public static final byte CMD_MELODY	= 34;	//	W
    public static final byte CMD_BUZZER	 =35;	//	W
    public static final byte CMD_IRREMOCON = 36;	//	R
    public static final byte CMD_BOTTOM_LINE_IR = 37;	//	R
    public static final byte CMD_MAGNET	= 38;	//	R
    public static final byte CMD_GYRO = 39;	//	R

    ///////////CMD Define end////////

    private byte[] M_NOW = new byte[10];
    private byte[] M_1Hz = new byte[10];
    private byte[] M_5Hz = new byte[10];
    private byte[] M_10Hz = new byte[10];

    private byte[] WHEEL_TARGET_V = new byte[4];
    private byte[] WHEEL_CURRENT_V = new byte[4];
    private byte[] WHEEL_ACC_DEACC = new byte[8];
    private byte[] TILT_TARGET_A = new byte[2];
    private byte[] TILT_CURRENT_A = new byte[2];
    private byte[] SERVO_TARGET_S_A = new byte[10];
    private byte[] SERVO_CURRENT_A = new byte[6];
    private byte[] LED_M5 = new byte[4];
    private byte[] DISTANCE_PSD = new byte[4];
    private byte[] ODOMETRY = new byte[8];
    private byte[] CLCD = new byte[32];
    private byte BUTTON;
    private byte BOTTOM_LINE_IR;
    private byte MELODY;
    private byte BUZZER;
    private byte IRREMOCON;

    private byte[] BT_REMOCON = new byte[8];
    private byte[] MAGNET = new byte[8];
    private byte[] GYRO = new byte[8];
    private byte[] PLATFORM_SET = new byte[32+40];
    private byte[] PLATFORM_STATUS = new byte[6+40];
    private byte[] ADMINISTOR_PACKET = new byte[2+40];
    private byte[] FLASH_D1 = new byte[12+40];
    private byte[] FLASH_D2 = new byte[100];

    public static final int PC_Buffer_Size = 300;
    public static final int PC_Data_Size = 256;

    private byte[] USART2R_PC_Buffer = new byte[PC_Buffer_Size+1];
    private byte[] USART2R_PC_Data = new byte[PC_Data_Size+1];
    private int U2R_CNT = 0;
    private int U2R_CNT_PRE = 0;
    private int U2R_WPOS = 0;
    private int U2R_RPOS = 0;

    private byte[] USART2T_PC_Data = new byte[PC_Data_Size+1];

    private byte T_Packet_Length = 0;
    private byte T_Packet_RW = 0;
    private byte T_Packet_CMD1 = 0;
    private byte T_Packet_CMD2 = 0;
    private byte T_Packet_Flag = 0;
    private byte T_R_CRC = 0;

    private byte rx1=0;
    private byte R_Packet_Length = 0;
    private byte R_Packet_RW = 0;
    private byte R_Packet_CMD1 = 0;
    private byte R_Packet_CMD2 = 0;
    private byte R_Packet_Flag = 0;
    private byte R_Packet_CMD_FLASH_D1 = 0;

    private byte Tilt_Flag = 0; // 내부적으로 1회만 돌리기 위한 플래그 들
    private byte LCD_Flag = 0;
    private byte Speaker_Flag = 0;
    private byte LED_Flag = 0;
    private byte Buzzer_Flag = 0;
    private byte Wheel_Flag = 0;
    private byte Servo_Flag = 0;
    private byte BT_Packet_Flag = 0;

    // added by bcc
    private UsbSerialDriver sDriver = null;

    Protocol(UsbSerialDriver driver) {
        sDriver = driver;
    }
    public void appendLog(String text) {
        File logFile = new File("sdcard/m5.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
            String format = s.format(new Date());

            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append("["+format+"]"+text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void USART1_RX_vect(byte[] data){
        byte R_R_CRC;
        byte R_C_CRC = 0;

        /* debug
        String hex = "";
        for(int i = 0; i < data.length; i++){
            hex += String.format("%02x ", data[i]);
        }
        appendLog(data.length+" Read: "+hex);
        */
        for(int i = 0; i < data.length; i++){
            rx1 = data[i];
            if(U2R_WPOS > PC_Buffer_Size){
                U2R_WPOS = 0;
            }
            USART2R_PC_Buffer[U2R_WPOS] = rx1;

            if (USART2R_PC_Buffer[0] != 'M')
                U2R_WPOS = 0; //헤더 첫 바이트 체크
            else
                U2R_WPOS++;

            if (U2R_WPOS == 2) {
                if (USART2R_PC_Buffer[1] != 'T') { //헤더 두번째 바이트 체크
                    U2R_CNT = 0;
                    U2R_WPOS = 0;
                    USART2R_PC_Buffer[0] = 0;
                    USART2R_PC_Buffer[1] = 0;
                }

            } else if (U2R_WPOS == 3) {  // 패킷 길이 저장
                R_Packet_Length = USART2R_PC_Buffer[2];
            } else if (U2R_WPOS == (R_Packet_Length + 8)) { // 패킷 총길이 확인 테일 확인
                if (USART2R_PC_Buffer[(R_Packet_Length + 7)] == (byte) 0xff) { // check casting
                    R_R_CRC = USART2R_PC_Buffer[(R_Packet_Length + 6)];
                    for (int j = 3; j < R_Packet_Length + 6; j++) {    // crc 연사
                        R_C_CRC = (byte) (R_C_CRC ^ USART2R_PC_Buffer[j]);
                    }
                    R_C_CRC &= 0x7F;

                    if (R_C_CRC == R_R_CRC) { // crc 체크
                        R_Packet_Flag = 1;

                        for (int j = 0; j < R_Packet_Length + 3; j++) { // 패킷 처리 버퍼로 카피
                            USART2R_PC_Data[j] = USART2R_PC_Buffer[j + 3];
                        }
                        CMD_PARSER();
                    }
                    U2R_CNT = 0;  // 수신 변수 초기화
                    U2R_WPOS = 0;
                    USART2R_PC_Buffer[0] = 0;
                    USART2R_PC_Buffer[1] = 0;

                } else {
                    U2R_CNT = 0; // 수신 변수 초기화
                    U2R_WPOS = 0;
                    USART2R_PC_Buffer[0] = 0;
                    USART2R_PC_Buffer[1] = 0;

                }
            } // if(U2R_WPOS == 2)
        } // end for (byte UDR1:
    }
    protected void CMD_PARSER(){ // 패킷 내의 데이터에 따른 동작 구분
        if(R_Packet_Flag == 1){
            R_Packet_Flag = 0;
            R_Packet_RW = USART2R_PC_Data[0];
            R_Packet_CMD1 = USART2R_PC_Data[1];
            R_Packet_CMD2 = USART2R_PC_Data[2];
            if(R_Packet_RW == 1){
                Packet_CMD1_Write(R_Packet_CMD1);
            }else if(R_Packet_RW == 0){
                CMD_Packet_Send(R_Packet_CMD1, (byte) 1);
            }
            R_Packet_CMD_FLASH_D1 = 1;
        }
    }
    protected void Packet_CMD1_Write(byte RCMD) { // 수신 데이터 버퍼를 내부 구조체로 카피
        switch(RCMD){
            case CMD_M_NOW:
                System.arraycopy(USART2R_PC_Data, 3, M_NOW, 0, M_NOW.length);
                break;
            case CMD_M_1Hz:
                System.arraycopy(USART2R_PC_Data, 3, M_1Hz, 0, M_1Hz.length);
                break;
            case CMD_M_5Hz:
                System.arraycopy(USART2R_PC_Data, 3, M_5Hz, 0, M_5Hz.length);
                break;
            case CMD_M_10Hz:
                System.arraycopy(USART2R_PC_Data, 3, M_10Hz, 0, M_10Hz.length);
                break;
            case CMD_PLATFORM_SET:
                System.arraycopy(USART2R_PC_Data, 3, PLATFORM_SET, 0, PLATFORM_SET.length);
                break;
            case CMD_PLATFORM_STATUS:
                System.arraycopy(USART2R_PC_Data, 3, PLATFORM_STATUS, 0, PLATFORM_STATUS.length);
                break;
            case CMD_ADMINISTOR_PACKET:
                System.arraycopy(USART2R_PC_Data, 3, ADMINISTOR_PACKET, 0, ADMINISTOR_PACKET.length);
                break;
            case CMD_FLASH_D1:
                System.arraycopy(USART2R_PC_Data, 3, FLASH_D1, 0, FLASH_D1.length);
                byte[] cmds = new byte[10]; // each element has 0 by default.
                cmds[0] = CMD_DISTANCE_PSD;
                setM_10Hz(cmds);
                break;
            case CMD_FLASH_D2:
                System.arraycopy(USART2R_PC_Data, 3, FLASH_D2, 0, FLASH_D2.length);
                break;
            case CMD_BT_REMOCON:
                System.arraycopy(USART2R_PC_Data, 3, BT_REMOCON, 0, BT_REMOCON.length);
                BT_Packet_Flag = 1;
                break;
            case CMD_WHEEL_TARGET_V:
                System.arraycopy(USART2R_PC_Data, 3, WHEEL_TARGET_V, 0, WHEEL_TARGET_V.length);
                Wheel_Flag = 1;
                break;
            case CMD_WHEEL_ACC_DEACC:
                System.arraycopy(USART2R_PC_Data, 3, WHEEL_ACC_DEACC, 0, WHEEL_ACC_DEACC.length);
                break;
            case CMD_TILT_TARGET_A:
                System.arraycopy(USART2R_PC_Data, 3, TILT_TARGET_A, 0, TILT_TARGET_A.length);
                Tilt_Flag = 1;
                break;
            case CMD_SERVO_TARGET_S_A:
                System.arraycopy(USART2R_PC_Data, 3, SERVO_TARGET_S_A, 0, SERVO_TARGET_S_A.length);
                Servo_Flag = 1;
                break;
            case CMD_LED_M5:
                System.arraycopy(USART2R_PC_Data, 3, LED_M5, 0, LED_M5.length);
                LED_Flag = 1;
                break;
            case CMD_CLCD:
                System.arraycopy(USART2R_PC_Data, 3, CLCD, 0, CLCD.length);
                LCD_Flag = 1;
                break;
            case CMD_MELODY:
                MELODY = USART2R_PC_Data[3];
                Speaker_Flag = 1;
                break;
            case CMD_BUZZER:
                BUZZER = USART2R_PC_Data[3];
                Buzzer_Flag = 1;
                break;
            case CMD_BUTTON:
                BUTTON = USART2R_PC_Data[3];
                break;
            case CMD_DISTANCE_PSD:
                System.arraycopy(USART2R_PC_Data, 3, DISTANCE_PSD, 0, DISTANCE_PSD.length);
                break;
            case CMD_WHEEL_CURRENT_V:
                System.arraycopy(USART2R_PC_Data, 3, WHEEL_CURRENT_V, 0, WHEEL_CURRENT_V.length);
                break;
            case CMD_TILT_CURRENT_A:
                System.arraycopy(USART2R_PC_Data, 3, TILT_CURRENT_A, 0, TILT_CURRENT_A.length);
                break;
            case CMD_SERVO_CURRENT_A:
                System.arraycopy(USART2R_PC_Data, 3, SERVO_CURRENT_A, 0, SERVO_CURRENT_A.length);
                break;
            case CMD_ODOMETRY:
                System.arraycopy(USART2R_PC_Data, 3, ODOMETRY, 0, ODOMETRY.length);
                break;
            case CMD_IRREMOCON:
                IRREMOCON = USART2R_PC_Data[3];
                break;
            case CMD_BOTTOM_LINE_IR:
                BOTTOM_LINE_IR = USART2R_PC_Data[3];
                break;
            case CMD_MAGNET:
                System.arraycopy(USART2R_PC_Data, 3, MAGNET, 0, MAGNET.length);
                break;
            case CMD_GYRO:
                System.arraycopy(USART2R_PC_Data, 3, GYRO, 0, GYRO.length);
                break;
            default:
                break;
        } // switch(RCMD
    }

   protected void CMD_Packet_Send(byte P_CMD, byte P_RW) { // 보내는 패킷 생성
        byte T_C_CRC = 0;
        byte i;

        USART2T_PC_Data[0] = 'M';
        USART2T_PC_Data[1] = 'T';


        Packet_CMD1_Read(P_CMD); // 커맨드 별로 별도 구조체 선언되어있기때문에 구분하기 위함

        USART2T_PC_Data[3] = P_RW;
        USART2T_PC_Data[4] = P_CMD;
        USART2T_PC_Data[5] = 0;//reservation

        for(i=3;i<(USART2T_PC_Data[2] + 6);i++){
            T_C_CRC ^= USART2T_PC_Data[i];
        }
        T_C_CRC &= 0x7F;

        USART2T_PC_Data[USART2T_PC_Data[2] + 6] = T_C_CRC;
        USART2T_PC_Data[USART2T_PC_Data[2] + 7] = (byte) 0xFF;

       // added by bcc
       if(sDriver != null){
           try {
               for( i = 0; i < (USART2T_PC_Data[2] + 8); i++){
                   byte[] tmp = new byte[1];
                   tmp[0] = USART2T_PC_Data[i];
                   sDriver.write(tmp, 1);
               }
           } catch (IOException e) {
               e.printStackTrace();
               try {
                   sDriver.close();
               } catch (IOException e1) {
                   e1.printStackTrace();
               }
               sDriver = null;
           }
       }
       /* debug
       String hex = "";
       for( i = 0; i < (USART2T_PC_Data[2] + 8); i++){
            // put_u1_c( USART2T_PC_Data[i] );
            hex += String.format("%02x ", USART2T_PC_Data[i]);
        }
        appendLog(hex);
        */
    }
    protected void Packet_CMD1_Read(byte RCMD){
        // 구조체 사이즈 확인 -> 구조체 tx 버퍼로 카피
        switch(RCMD){
            case CMD_M_NOW:
                USART2T_PC_Data[2] = (byte) M_NOW.length;
                System.arraycopy(M_NOW, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_M_1Hz:
                USART2T_PC_Data[2] = (byte) M_1Hz.length;
                System.arraycopy(M_1Hz, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_M_5Hz:
                USART2T_PC_Data[2] = (byte) M_5Hz.length;
                System.arraycopy(M_5Hz, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_M_10Hz:
                USART2T_PC_Data[2] = (byte) M_10Hz.length;
                System.arraycopy(M_10Hz, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//

            case CMD_PLATFORM_SET:
                USART2T_PC_Data[2] = (byte) PLATFORM_SET.length;
                System.arraycopy(PLATFORM_SET, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_PLATFORM_STATUS:
                USART2T_PC_Data[2] = (byte) PLATFORM_STATUS.length;
                System.arraycopy(PLATFORM_STATUS, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_ADMINISTOR_PACKET	:
                USART2T_PC_Data[2] = (byte) ADMINISTOR_PACKET.length;
                System.arraycopy(ADMINISTOR_PACKET, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_FLASH_D1			:
                USART2T_PC_Data[2] = (byte) FLASH_D1.length;
                System.arraycopy(FLASH_D1, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_FLASH_D2			:
                USART2T_PC_Data[2] = (byte) FLASH_D2.length;
                System.arraycopy(FLASH_D2, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_BT_REMOCON			:
                USART2T_PC_Data[2] = (byte) BT_REMOCON.length;
                System.arraycopy(BT_REMOCON, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_WHEEL_TARGET_V		:
                USART2T_PC_Data[2] = (byte) WHEEL_TARGET_V.length;
                System.arraycopy(WHEEL_TARGET_V, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_WHEEL_ACC_DEACC		:
                USART2T_PC_Data[2] = (byte) WHEEL_ACC_DEACC.length;
                System.arraycopy(WHEEL_ACC_DEACC, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_TILT_TARGET_A		:
                USART2T_PC_Data[2] = (byte) TILT_TARGET_A.length;
                System.arraycopy(TILT_TARGET_A, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_SERVO_TARGET_S_A	:
                USART2T_PC_Data[2] = (byte) SERVO_TARGET_S_A.length;
                System.arraycopy(SERVO_TARGET_S_A, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_LED_M5				:
                USART2T_PC_Data[2] = (byte) LED_M5.length;
                System.arraycopy(LED_M5, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_DISTANCE_PSD		:
                USART2T_PC_Data[2] = (byte) DISTANCE_PSD.length;
                System.arraycopy(DISTANCE_PSD, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_CLCD				:
                USART2T_PC_Data[2] = (byte) CLCD.length;
                System.arraycopy(CLCD, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_BUTTON				:
                USART2T_PC_Data[2] = 1;
                USART2T_PC_Data[6] = BUTTON;
                break;//
            case CMD_MELODY				:
                USART2T_PC_Data[2] = 1;
                USART2T_PC_Data[6] = MELODY;
                break;//
            case CMD_BUZZER				:
                USART2T_PC_Data[2] = 1;
                USART2T_PC_Data[6] = BUZZER;
                break;//
            case CMD_WHEEL_CURRENT_V		:
                USART2T_PC_Data[2] = (byte) WHEEL_CURRENT_V.length;
                System.arraycopy(WHEEL_CURRENT_V, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_TILT_CURRENT_A		:
                USART2T_PC_Data[2] = (byte) TILT_CURRENT_A.length;
                System.arraycopy(TILT_CURRENT_A, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_SERVO_CURRENT_A		:
                USART2T_PC_Data[2] = (byte) SERVO_CURRENT_A.length;
                System.arraycopy(SERVO_CURRENT_A, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_ODOMETRY			:
                USART2T_PC_Data[2] = (byte) ODOMETRY.length;
                System.arraycopy(ODOMETRY, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_IRREMOCON			:
                USART2T_PC_Data[2] = 1;
                USART2T_PC_Data[6] = IRREMOCON;
                break;//
            case CMD_BOTTOM_LINE_IR		:
                USART2T_PC_Data[2] = 1;
                USART2T_PC_Data[6] = BOTTOM_LINE_IR;
                break;//
            case CMD_MAGNET				:
                USART2T_PC_Data[2] = (byte) MAGNET.length;
                System.arraycopy(MAGNET, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
            case CMD_GYRO				:
                USART2T_PC_Data[2] = (byte) GYRO.length;
                System.arraycopy(GYRO, 0, USART2T_PC_Data, 6, USART2T_PC_Data[2]);
                break;//
        }
    }
    public void setWHEEL_TARGET_V(short wheel1, short wheel2){
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(4);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer = buffer.putShort(wheel1);
        buffer = buffer.putShort(wheel2);
        WHEEL_TARGET_V = buffer.array();
        CMD_Packet_Send(CMD_WHEEL_TARGET_V, (byte) 1);
    }
    public void setWHEEL_CURRENT_V(short wheel1, short wheel2){
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(4);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer = buffer.putShort(wheel1);
        buffer = buffer.putShort(wheel2);
        WHEEL_CURRENT_V = buffer.array();
        CMD_Packet_Send(CMD_WHEEL_CURRENT_V, (byte) 1);
    }
    public ByteBuffer getWHEEL_ACC_DEACC(){
        ByteBuffer buffer = ByteBuffer.wrap(WHEEL_ACC_DEACC);
        return buffer;
    }
    public void setTILT_TARGET_A(short position) {
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(2);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer = buffer.putShort(position);
        TILT_TARGET_A = buffer.array();
        CMD_Packet_Send(CMD_TILT_TARGET_A, (byte) 1);
    }
    public void setTILT_CURRENT_A() {
        CMD_Packet_Send(CMD_TILT_CURRENT_A, (byte) 0);
    }
    public ByteBuffer getTILT_CURRENT_A(){
        ByteBuffer buffer = ByteBuffer.wrap(TILT_CURRENT_A);
        return buffer;
    }
    private void setM_10Hz(byte[] cmds){
        M_10Hz = cmds;
        CMD_Packet_Send(CMD_M_10Hz, (byte) 1);
    }
    private void setM_5Hz(byte[] cmds){
        M_5Hz = cmds;
        CMD_Packet_Send(CMD_M_5Hz, (byte) 1);
    }
    private void setM_1Hz(byte[] cmds){
        M_1Hz = cmds;
        CMD_Packet_Send(CMD_M_1Hz, (byte) 1);
    }
    public void setSERVO_TARGET_S_A(byte[] targets){
        SERVO_TARGET_S_A = targets;
        CMD_Packet_Send(CMD_SERVO_TARGET_S_A, (byte) 1);
    }
    public void setSERVO_TARGET_S_A(short speed, short id1, short id2, short id3){
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(10);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer = buffer.putShort(speed);
        buffer = buffer.putShort((short) 0);
        buffer = buffer.putShort(id1);
        buffer = buffer.putShort(id2);
        buffer = buffer.putShort(id3);
        SERVO_TARGET_S_A = buffer.array();
        CMD_Packet_Send(CMD_SERVO_TARGET_S_A, (byte) 1);
    }
    public void setLED_M5(byte[] leds){
        LED_M5 = leds;
        CMD_Packet_Send(CMD_LED_M5, (byte) 1);
    }
    public void setLED_M5(byte r, byte g, byte b, byte onoff){
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(4);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer = buffer.put(r);
        buffer = buffer.put(g);
        buffer = buffer.put(b);
        buffer = buffer.put(onoff);
        LED_M5 = buffer.array();
        CMD_Packet_Send(CMD_LED_M5, (byte) 1);
    }
    public void setMELODY(byte trigger){
        MELODY = trigger;
        CMD_Packet_Send(CMD_MELODY, (byte) 1);
    }
    public void setBUZZER(byte HEXA){
        BUZZER = HEXA;
        CMD_Packet_Send(CMD_BUZZER, (byte) 1);
    }

    public void setBT_REMOCON(byte[] cmds){
        BT_REMOCON = cmds;
        CMD_Packet_Send(CMD_BT_REMOCON, (byte) 1);
    }
    public void setIRREMOCON(byte HEXA){
        IRREMOCON = HEXA;
        CMD_Packet_Send(CMD_IRREMOCON, (byte) 1);
    }
    public void setBT_REMOCON(short x, short y, short keyh, short keyl){
        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(8);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer = buffer.putShort(x);
        buffer = buffer.putShort(y);
        buffer = buffer.putShort(keyh);
        buffer = buffer.putShort(keyl);
        BT_REMOCON = buffer.array();
        CMD_Packet_Send(CMD_BT_REMOCON, (byte) 1);
    }
    public ByteBuffer getDISTANCE_PSD(){
        ByteBuffer buffer = ByteBuffer.wrap(DISTANCE_PSD);
        return buffer;
    }
    public byte getBOTTOM_LINE_IR(){
        return BOTTOM_LINE_IR;
    }
    public ByteBuffer getMAGNET(){
        ByteBuffer buffer = ByteBuffer.wrap(MAGNET);
        return buffer;
    }
    public ByteBuffer getODOMETRY(){
        ByteBuffer buffer = ByteBuffer.wrap(ODOMETRY);
        return buffer;
    }
    public ByteBuffer getGYRO(){
        ByteBuffer buffer = ByteBuffer.wrap(GYRO);
        return buffer;
    }
    public ByteBuffer getFLASH_D1(){
        ByteBuffer buffer = ByteBuffer.wrap(FLASH_D1);
        appendLog(String.format("model : "+buffer.getShort()));
        appendLog(String.format("model_branch : "+buffer.getShort()));
        appendLog(String.format("firmware_version : "+buffer.getShort()));
        appendLog(String.format("protocol_version : "+buffer.getShort()));
        appendLog(String.format("production : "+buffer.getShort()));
        appendLog(String.format("serial : "+buffer.getShort()));
        return buffer;
    }
    public boolean recvRespMessage(StringBuffer recordValue) {
        if(R_Packet_CMD_FLASH_D1 == 1 ){
            R_Packet_CMD_FLASH_D1 = 0;
            ByteBuffer s;
            s = getDISTANCE_PSD();
            recordValue.append(String.format("m5_psd/frontL %d\n", s.get()));
            recordValue.append(String.format("m5_psd/frontR %d\n", s.get()));
            recordValue.append(String.format("m5_psd/left %d\n", s.get()));
            recordValue.append(String.format("m5_psd/right %d\n", s.get()));
            /*byte ir = getBOTTOM_LINE_IR();
            recordValue.append(String.format("m5_ir %d\n", ir));
            s = getODOMETRY();
            recordValue.append(String.format("m5_odometry/x %d\n", s.getShort()));
            recordValue.append(String.format("m5_odometry/y %d\n", s.getShort()));
            recordValue.append(String.format("m5_odometry/a %d\n", s.getShort()));
            recordValue.append(String.format("m5_odometry/v %d\n", s.getShort()));

            s = getGYRO();
            recordValue.append(String.format("m5_gyro/roll %d\n", s.getShort()));
            recordValue.append(String.format("m5_gyro/pitch %d\n", s.getShort()));
            recordValue.append(String.format("m5_gyro/yaw %d\n", s.getShort()));
            recordValue.append(String.format("m5_gyro/tem %d\n", s.getShort()));
            s = getWHEEL_ACC_DEACC();
            recordValue.append(String.format("m5_get_velo/left %d\n", s.getShort()));
            recordValue.append(String.format("m5_get_velo/right %d\n", s.getShort()));
            */
            return true;
        }
        return false;
    }
}
